# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit calculate

DESCRIPTION="Wallpapers for CLOT"
HOMEPAGE="http://31.41.246.91/my/splash-clot"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"

SRC_URI="ftp://31.41.246.91/my/splash-clot/splash-clot-15.6.tar.bz2"


RDEPEND="media-gfx/splashutils"

src_install() {
	insinto /
	doins -r .
}
