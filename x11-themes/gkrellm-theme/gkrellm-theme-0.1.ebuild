# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-themes/gkrellm-themes/gkrellm-themes-0.1.ebuild,v 1.18 2015/05/14 16:29:05 mr_bones_ Exp $

EAPI=5
DESCRIPTION="my theme for GKrellM"
HOMEPAGE="ftp://10.202.32.5/my/gkrellm/"
THEME_URI="ftp://10.202.32.5/my/gkrellm"
SRC_URI="${THEME_URI}/invisible1.tar.gz"

LICENSE="freedist"
SLOT="0"
KEYWORDS="amd64 ~mips ppc ppc64 sparc x86"
IUSE=""
RESTRICT="strip"

DEPEND=""
RDEPEND=">=app-admin/gkrellm-2.1"

src_unpack() {
	mkdir "${S}"
	cd "${S}"
	for theme in ${SRC_URI} ; do
		unpack $(basename $theme)
	done
}

src_compile() { :; }

src_install() {
	dodir /usr/share/gkrellm2/themes/
	keepdir /usr/share/gkrellm2/themes/
	cd "${S}"
	ewarn "Please ignore any errors that may appear!"
	chmod -R g-sw+rx *
	chmod -R o-sw+rx *
	chmod -R u-s+rwx *
	cp -pR * "${D}"/usr/share/gkrellm2/themes/
}
